
class Pomodoro {
  constructor(bus) {
    this.bus = bus
    this.message    

    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.start_requested', this.start.bind(this))
    this.bus.subscribe('timer.askTimeLeft', this.calculateTimeLeft.bind(this))
  }

  start() {
    this.message = { minutes: 25 }

    this.bus.publish('timer.start', this.message)
  }

  calculateTimeLeft() {
    
    if (this.message.minutes > 0) {
      this.message.minutes--;
      console.log('sí se está ejecutando el countdown');

         }
    this.bus.publish('timer.timeLeft', this.message)
  }
}

export default Pomodoro
